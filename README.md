## About

To Learn more, visit https://developer.gladefinance.co/docs

## Getting Started 
You need a Merchant ID and Key to authenticate against the API, please contact support@gladefinance.co to setup a demo account.

## Installation
You can close this project and include the class in your project or you can add the repo in your composer.json file as in the example below:


````
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/gladepay-apis/gladephp"
        }
    ],
    "require": {
        "gladefinance/gladephp": "^0.0.1"
    }
}

````

Or simply run the 

```` 
composer require gladefinance/gladephp 

```` 
in your project.


## Set environment

```` 
require './vendor/autoload.php'; 
````

For Production environment
```` 
$api = new Glade($merchantId, $merchantKey, true); 
````

For Sandbox environment
```` 
$api = new Glade($merchantId, $merchantKey, false); 
````

## Methods
BVN Verification
```` 
$api->bvnValidation(string $bvnNumber); 
````


Get Chargeable Banks
```` 
$api->supportedChargeableBanks(); 
````

Get Bank List
```` 
$api->bankList(); 
````

Verify Account Name
```` 
$api->verifyAccountName(string $accountNumber, string $bankCode, string $bankName); 
````

Get Personalized Account Info
```` 
$api->personalizedAccount(string $accountName, string $accountEmail, string $accountBvn, string $reference, string $channel = "providus"); 
````

Create Customer
```` 
$api->createCustomer(string $name, string $email, string $phoneNumber,  string $address); 
````

Get All Customers
```` 
$api->getCustomers(); 
````

Get A Single Customer's Details
```` 
$api->getCustomerDetail(int $customerId); 
````

Get All Bill Categories
```` 
$api->getBillCategory(string $category = null); 
````

Get Bill ID
```` 
$api->getBillById(int $billId); 
````

Resolve Bill
```` 
$api->resolveBill(string $payCode, string $reference); 
````

Purchase Bill
```` 
$api->purchaseBill(string $payCode, int $amount, string $reference, string $orderReference = null); 
````

Verify Bill Purchase
```` 
$api->verifyBillPurchase(string $transactionReference); 
````

Make A Single Transfer
```` 
$api->transfer(int $amount, string $receiverAccountNumber, string $receiverBankCode, string $senderName, string $reference, string $narration);
````

Verify Single Transfer
```` 
$api->verifySingleTransfer(string $reference); 
````

Make Bulk Transfer
```` 
$api->bulkTransfer(array $transferObjects); 
````

Create Payment Link
````
 $api->createPaymentLink(string $title, string $description, int $amount, string $type, bool $payerBearsFees, bool $acceptNumber, string $notificationEmail, string $customLink = null, string $redirectUrl = null, string $customMessage = null, string $frequency = null); 
 ````

Create Ticket
````  
$api->createTicket(string $title, string $description, int $amount, string $type, bool $payerBearsFees, bool $acceptNumber, string $notificationEmail, array $ticketData, string $customLink = null, string $redirectUrl = null, string $customMessage = null, string $frequency = null); 
````

Create Invoice
```` 
$api->invoice(int $customerId,bool $chargeUser, int $shipping, int $vat, string $dueDate,  bool $allowedDiscount, array $invoiceItems, string $note, string $discountType= null, string $invoiceId); 
````


## Return Values
All methods return an array.
