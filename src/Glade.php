<?php

namespace Gladefinance\Core;

use Error;
use Exception;
use Gladefinance\Core\Utils\Constant;
use Gladefinance\Core\Utils\Helper;
use Gladefinance\Core\Utils\Request;

class Glade
{

    protected string $merchantId;
    protected string $merchantKey;
    protected string $baseUrl;
    protected bool $isProduction;
    protected Request $request;

    public function __construct(string $merchantId, string $merchantKey, bool $isProduction = false)
    {
        $this->merchantId = $merchantId;
        $this->merchantKey = $merchantKey;
        $this->baseUrl = $isProduction ? Constant::$PRODUCTION_ENVIRONMENT : Constant::$SANBOX_ENVIRONMENT;

        $this->request = new Request($this->merchantId, $this->merchantKey, $this->baseUrl);
    }


    public function bvnValidation(string $bvnNumber): object
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            'inquire' => 'bvn',
            'bvn' => $bvnNumber
        ]);
    }

    public function supportedChargeableBanks(): object
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            'inquire' => 'supported_chargable_banks'
        ]);
    }

    public function bankList()
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            'inquire' => 'banks'
        ]);
    }

    public function verifyAccountName(string $accountNumer, string $bankCode, string $bankName)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            "inquire" => "accountname",
            "accountnumber" => $accountNumer,
            "bankcode" => $bankCode,
            "bankname" => $bankName
        ]);
    }

    public function personalizedAccount(string $accountName, string $accountEmail, string $accountBvn, string $reference, string $channel = "providus")
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            "request" => "personalized-accounts",
            "name" => $accountName,
            "reference" => $reference,
            "email" => $accountEmail,
            "bvn" => $accountBvn,
            "bank" => $channel
        ]);
    }

    public function createCustomer(string $name, string $email, string $phoneNumber,  string $address)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            "request" => "customer",
            "address" => $address,
            "email" => $email,
            "name" => $name,
            "phone" => $phoneNumber
        ]);
    }

    public function getCustomers()
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            "inquire" => "customer"
        ]);
    }

    public function getCustomerDetail(int $customerId)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            "inquire" => "customer_detail",
            "customer_id" => $customerId
        ]);
    }

    public function getBillCategory(string $category = null)
    {
        $data = [
            "action" => "pull"
        ];
        if ($category) {
            $data["category"] = $category;
        }
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$BILL_URL, $data);
    }

    public function getBillById(int $billId)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$BILL_URL, [
            "action" => "pull",
            "bills_id" => $billId
        ]);
    }

    public function resolveBill(string $payCode, string $reference)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$BILL_URL, [
            "action" => "resolve",
            "paycode" => $payCode,
            "reference" => $reference
        ]);
    }

    public function purchaseBill(string $payCode, int $amount, string $reference, string $orderReference = null)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$BILL_URL, [
            "action" => "resolve",
            "paycode" => $payCode,
            "reference" => $reference,
            "amount" => $amount,
            "orderRef" => $orderReference
        ]);
    }

    public function verifyBillPurchase(string $transactionReference)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$BILL_URL, [
            "action" => "verify",
            "txnRef" => $transactionReference
        ]);
    }


    public function transfer(int $amount, string $receiverAccountNumber, string $receiverBankCode, string $senderName, string $reference, string $narration)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$TRANSFER_URL, [
            "action" => "transfer",
            "amount" => $amount,
            "accountnumber" => $receiverAccountNumber,
            "bankcode" => $receiverBankCode,
            "sender_name" => $senderName,
            "narration" => $narration,
            "orderRef" => $reference
        ]);
    }

    public function verifySingleTransfer(string $reference)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$TRANSFER_URL, [
            "action" => "verify",
            "txnRef" => $reference,
        ]);
    }


    public function bulkTransfer(array $transferObjects)
    {
        if (!Helper::validateMultiArray($transferObjects)) {
            return Request::handleCustomResponse(400, 'error', 'array object must be a multidimensional array');
        }

        if (!Helper::validateBulkTransferArray($transferObjects)) {

            return Request::handleCustomResponse(400, 'error', 'Data structure does not match required data, please refer to documentation');
        }

        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$TRANSFER_URL, [
            "action" => "transfer",
            "type" => "bulk",
            "data" => $transferObjects
        ]);
    }



    public function createPaymentLink(string $title, string $description, int $amount, string $type, bool $payerBearsFees, bool $acceptNumber, string $notificationEmail, string $customLink = null, string $redirectUrl = null, string $customMessage = null, string $frequency = null)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            "request" => "paylink",
            "title" => $title,
            "description" => $description,
            "amount" => $amount,
            "currency" => "NGN",
            "type" => $type,
            "payer_bears_fees" => $payerBearsFees,
            "accept_number" => $acceptNumber,
            "custom_link" => $customLink,
            "redirect_url" => $redirectUrl,
            "custom_message" => $customMessage,
            "notification_email" => $notificationEmail,
            "frequency" => $frequency
        ]);
    }
    
    public function createTicket(string $title, string $description, int $amount, string $type, bool $payerBearsFees, bool $acceptNumber, string $notificationEmail, array $ticketData, string $customLink = null, string $redirectUrl = null, string $customMessage = null, string $frequency = null)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            "request" => "ticket",
            "title" => $title,
            "description" => $description,
            "amount" => $amount,
            "currency" => "NGN",
            "type" => $type,
            "payer_bears_fees" => $payerBearsFees,
            "accept_number" => $acceptNumber,
            "custom_link" => $customLink,
            "redirect_url" => $redirectUrl,
            "custom_message" => $customMessage,
            "notification_email" => $notificationEmail,
            "frequency" => $frequency,
            "ticket_data" => $ticketData
        ]);
    }

    public function invoice(int $customerId,bool $chargeUser, int $shipping, int $vat, string $dueDate,  bool $allowedDiscount, array $invoiceItems, string $note, string $discountType= null, string $invoiceId)
    {
        return $this->request->makeApiCall(Constant::$PUT_METHOD, Constant::$RESOURCE_URL, [
            "request"=> "invoice",
            "invoice_id"=> $invoiceId,
            "currency"=> "NGN",
            "customer_id"=> $customerId,
            "date_due"=> $dueDate,
            "discount_type"=> $discountType,
            "discount"=> $allowedDiscount,
            "shipping"=> $shipping,
            "vat"=> $vat,
            "note"=> $note,
            "charge_user"=> $chargeUser,
            "items"=>  $invoiceItems
        ]);
    }
}
