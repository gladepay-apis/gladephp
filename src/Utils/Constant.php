<?php

namespace Gladefinance\Core\Utils;

class Constant {

    public static string $POST_METHOD = "POST";
     public static string $GET_METHOD = "GET";
     public static string $PATCH_METHOD = "PATCH";
     public static string $PUT_METHOD = "PUT";
     public static string $DELETE_METHOD = "DELETE";


     public static string $LINK_CHANNEL = "link";
     public static string $INVOICE_CHANNEL = "invoice";
     public static string $TICKET_CHANNEL = "ticket";


     public static string $SANBOX_ENVIRONMENT = "https://api-v2-testing.gladefinance.co/";
     public static string $PRODUCTION_ENVIRONMENT = "https://api.gladefinance.co/";


     public static string $RESOURCE_URL = "resources";
     public static string $BILL_URL = "bills";
     public static string $TRANSFER_URL = "disburse";

}