<?php

namespace Gladefinance\Core\Utils;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;
use Psr\Http\Message\ResponseInterface;
use stdClass;

class Request
{


    protected string $merchantId;
    protected string $merchantKey;
    protected string $environment;
    protected string $baseUrl;
    protected Client $client;

    public function __construct(string $merchantId, string $merchantKey, string $baseUrl)
    {
        $this->merchantId = $merchantId;
        $this->merchantKey = $merchantKey;
        $this->baseUrl =  $baseUrl;

        $this->client = new Client([
            'base_uri' => $this->baseUrl,
            'headers' => $this->setHeaders(),
            'verify' => false
        ]);
    }


    protected function setHeaders()
    {
        return [
            'mid' => $this->merchantId,
            'key' => $this->merchantKey,
            'accept' => "application/json",
            'content-type' => "application/json",
        ];
    }

    /**
     * Handle all http calls
     * 
     * @param string $method
     * @param string $url
     * @param array $payload
     * 
     * @return object
     */
    public function makeApiCall(string $method, string $url, array $payload = []): object
    {
        try {
            switch ($method) {
                case Constant::$POST_METHOD:
                    $response = $this->client->post($url, ['body' => json_encode($payload)]);
                    break;
                case Constant::$GET_METHOD:
                    $response = $this->client->get($url);
                    break;
                case Constant::$PATCH_METHOD:
                    $response = $this->client->patch($url, ['body' => json_encode($payload)]);
                    break;
                case Constant::$PUT_METHOD:
                    $response = $this->client->put($url, ['body' => json_encode($payload)]);
                    break;
                case Constant::$DELETE_METHOD:
                    $response = $this->client->delete($url);
                    break;
                default:
                    $response = $this->client->get($url, $payload);
            }

            return $this->handleSuccessResponse($response);
        } catch (ClientException $exception) {
            return $this->handleFailureResponse($exception);
        }
    }

    /**
     * Handle success response
     * 
     * @param ResponseInterface $data
     * 
     * @return object
     */
    protected function handleSuccessResponse(ResponseInterface $data) : object
    {
        $responsePayload = $data->getBody()->getContents();
        $response =  new stdClass(); 
        $response->code = $data->getStatusCode();
        $response->status = 'success';
        $response->data = $responsePayload;

        return $response;
    }

    /**
     * Handle error response
     * 
     * @param ClientException $exception
     * 
     * @return object
     */
    protected function handleFailureResponse(ClientException $exception) : object
    {
        $responsePayload = $exception->getResponse();
        $response =  new stdClass();
        $response->code = $responsePayload->getStatusCode();
        $response->status = 'error';
        $response->message = ($response->code === 401 || $response->code == '401') ? 'Ensure merchant key and merchant id is provided' : json_decode($responsePayload->getBody()->getContents(), true)['message'];

        return $response;
    }

    /**
     * Handle custom response
     * 
     * @param int $code
     * @param string $code
     * @param string $message
     * 
     * @return object
     */
    public static function handleCustomResponse(int $code, string $status, string $message) : object
    {
        $response =  new stdClass();
        $response->code = $code;
        $response->status = $status;
        $response->message = $message;
        return $response;
    }
}
