<?php

namespace Gladefinance\Core\Utils;

class Helper {


    public static function validateMultiArray(array $array)
    {
        rsort( $array );
        return isset( $array[0] ) && is_array( $array[0] );
    }

    public static function validateBulkTransferArray(array $arrayOfObjects)
    {
        $transferData = ["amount","accountnumber", "bankcode", "sender_name", "narration","orderRef"];
        foreach($arrayOfObjects as $array)
        {
            if(array_diff_key(array_flip($transferData), $array))
            {
                return false;
            }
        }
        return true;
    }

}